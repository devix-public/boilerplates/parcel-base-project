const render = async () => {
  await import('../js/common');
  const { capitalizeFirstLetter } = await import('../js/string-utils');

  console.log(capitalizeFirstLetter('about'));
};

render();
