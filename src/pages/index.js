const render = async () => {
  await import('../js/common');
  const { capitalizeFirstLetter } = await import('../js/string-utils');

  console.log(capitalizeFirstLetter('index'));
};

render();

/*
// PODERIA SER ASSIM, MAS FICARIA SEM CODE SPLIT:

import '../styles/common.scss';
import './index.scss';
import '../js/common';
import { capitalizeFirstLetter } from '../js/string-utils';

console.log(capitalizeFirstLetter('index'));

*/
